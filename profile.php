<!DOCTYPE html>
<html lang="en">
<head>
  <title>Form wizard</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="css/form.css">
  <link rel="stylesheet" href="css/agency.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="js/form.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <!--favicon-->
       
     <link href="images/favicon-32x32.png" rel="shortcut icon" type="image/x-icon">
     
</head>
<body>

<div class="container"></div>,<div class="container">
  
<div class="stepwizard col-md-offset-3">
    <div class="stepwizard-row setup-panel">
      <div class="stepwizard-step">
        <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
        <p>Step 1</p>
      </div>
      <div class="stepwizard-step">
        <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
        <p>Step 2</p>
      </div>
      <div class="stepwizard-step">
        <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
        <p>Step 3</p>
      </div>
    </div>
  </div>
  
  <form role="form" action="" method="post">
    <div class="row setup-content" id="step-1">
      <div class="col-xs-6 col-md-offset-3">
        <div class="col-md-12">
          <h3> Step 1</h3>
          <div class="form-group">
            <label class="control-label">First Names</label>
            <input maxlength="100" name = "firstname" type="text" required="required" class="form-control" placeholder="Enter First Name">
          </div></input>
          <div class="form-group">
            <label class="control-label">Last Name</label>
            <input maxlength="100" type="text"  name = "lastname" required="required" class="form-control" placeholder="Enter Last Name">
          </div></input>
          <div class="form-group">
            <label class="control-label">Email</label>
            <input maxlength="100" type="text" name = "email" required="required" class="form-control" placeholder="Enter your email"></input>
          </div>
          <div class="form-group">
            <label class="control-label">Password</label>
            <input maxlength="100" type="text" name = "password" required="required" class="form-control" placeholder="Enter your password"></input>
          </div>
          <div class="form-group">
            <label class="control-label">Phone Number</label>
            <input maxlength="100" type="text" name = "phonenumber" required="required" class="form-control" placeholder="Enter your phone number"></input>
          </div>
          <button class="btn btn-primary nextBtn btn-lg pull-right" type="button">Next</button>
        </div>
      </div>
    </div>
    <div class="row setup-content" id="step-2">
      <div class="col-xs-6 col-md-offset-3">
        <div class="col-md-12">
          <h3> Step 2</h3>
          <div class="form-group">

            <label class="control-label">Job Category</label>
              <select style="margin-top: 10px;" class="form-control" name="jobcategory">
         <option>Accounting & Auditing</option>
         <option>Executive / Top Management</option>
         <option>Creatives (Arts, Design, Fashion)</option>
         <option>Customer Service & Customer Support</option>
         <option>Education & Training</option>
         <option>Engineering/Mechanics/Construction</option>
         <option>Farming & Agriculture</option>
         <option>Government</option>
         <option>Healthcare & Pharmaceuticals</option>
         <option>Human Resources & Recruitment</option>
         <option>Insuarance</option>
         <option>IT & Telecoms</option>
         <option>Legal</option>
         <option>Manufacturing/Production</option>
         <option>Marketing, Communications & PR</option>
         <option>Mining & Natural Resources</option>
         <option>NG0, Community & Social Development</option>
         <option>Administration & Office Support</option>
         <option>Project/Programme Management</option>
         <option>Research, Quality Control/Quality Assuarance</option>
         <option>Retail</option>
         <option>Sales/Business Development</option>
         <option>Security & Consulting</option>
         <option>Tourism & Travel</option>
         <option>Trade & Services</option>
         <option>Transport, Logistics, Procurement</option>
         <option>Real Estate</option>
         <option>Hospitality/Leisure/Travel</option>
         <option>Others</option>

    </select>
          </div>

          <div class="form-group">
            <label class="control-label">Experience</label>
            <textarea required="required" class="form-control" placeholder="Enter your work experience"></textarea>
          </div>
          <div class="form-group">
            <label class="control-label">Location</label>
             <textarea required="required" class="form-control" placeholder="Enter your location"></textarea>
          </div>
          <div class="form-group">
            <label class="control-label">Career Summary</label>
            <textarea required="required" class="form-control" placeholder="Enter your career summary"></textarea>
          </div>
          <button class="btn btn-primary prevBtn btn-lg pull-left" type="button">Previous</button>
          <button class="btn btn-primary nextBtn btn-lg pull-right" type="button">Next</button>
        </div>
      </div>
    </div>
    <div class="row setup-content" id="step-3">
      <div class="col-xs-6 col-md-offset-3">
        <div class="col-md-12">
          <h3> Step 3</h3>
          <div class="form-group">
            <label class="control-label">Education</label>
             <select style="margin-top: 10px;" class="form-control" name="jobcategory">
          <option>Select level of Education</option>   
         <option>Primary School</option>
         <option>Secondary School</option>
         <option>College</option>
         <option>Undergraduate</option>
         <option>Post graduate</option>
            </select>
          </div>
          <div class="form-group">
            <label class="control-label">Name of School</label>
            <input maxlength="100" type="text" required="required" class="form-control" placeholder="Enter your school name"></input>
          </div>

          <div class="form-group">
            <label class="control-label">Qualifications</label>
            <input maxlength="100" type="text" required="required" class="form-control" placeholder="Enter your qualifications"></input>
          </div>
          <div class="form-group">
            <label class="control-label">Certifications</label>
            <textarea required="required" class="form-control" placeholder="Enter your certifications"></textarea>
          </div>
          <div class="form-group">
            <label class="control-label">Referees</label>
            <textarea required="required" class="form-control" placeholder="Enter your referees"></textarea>
          </div>
          <button class="btn btn-primary prevBtn btn-lg pull-left" type="button">Previous</button>
          <button class="btn btn-success btn-lg pull-right" type="submit">Submit</button>
        </div>
      </div>
    </div>
  </form>

  
  
</div>
</body>
</html>







   